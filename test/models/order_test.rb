require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  test "an order can be created" do
    assert_difference 'Order.count', +1 do
      order = FactoryGirl.build :order_with_deliveries
      # order.delivered_at = Date.today + 1 #params[:delivered_at]
      assert order.save
      assert_equal order.price, order.deliveries.sum { |d| d.price }
      assert_operator order.price.fractional, :>, 0
      # assert_equal 2, order.deliveries.first.errors
    end
  end

  test "all orders have valid delivery date (not outdated)" do
    assert true
  end
  test "all orders have valid positive number of order_item" do
    assert true
  end

  # test "an order can have many entries with diff preferences" do
  #   box = FactoryGirl.build :box,
  #     preferences: [
  #       FactoryGirl.build(:box_preference),
  #       FactoryGirl.build(:box_preference)
  #     ]
  #   params = {
  #     box: box,
  #     quantity: 2,
  #     # delivered_at: Date.today + 1,
  #     preferences: [
  #       { box_preference: box.preferences.first },
  #       { box_preference: box.preferences.last }
  #     ]
  #   }
  #   assert_difference 'Order.count', +1 do
  #     # order = FactoryGirl.build :order
  #     # order.delivered_at = Date.today + 1
  #     order = Order.new params
  #     assert order.save
  #     assert_equal 1, order.entries.count
  #     assert_equal 1, order.entries.first.preferences.count
  #   end
  # end

  # test "a order_entry must has a delivered_at Date" do
  #   box = FactoryGirl.build :box,
  #     preferences: [ FactoryGirl.build(:box_preference) ]
  #   params = {
  #     box: box,
  #     quantity: 2,
  #     preferences: [
  #       { box_preference: box.preferences.first }
  #     ]
  #   }
  #   assert_difference 'Order.count', +0 do
  #     order = FactoryGirl.build :order
  #     # order.delivered_at = Date.today + 1
  #     order.entries.new params
  #     assert !order.save
  #     assert_equal 1, order.entries.count
  #     assert_equal 1, order.entries.first.preferences.count
  #   end
  # end

end
