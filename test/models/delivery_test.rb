require 'test_helper'

class DeliveryTest < ActiveSupport::TestCase
  test "a delivery can be created" do
    assert_difference 'Delivery.count', +1 do
      order = FactoryGirl.build :order
      order.deliveries = [ FactoryGirl.build(:delivery_with_items) ]
      # order.delivered_at = Date.today + 1 #params[:delivered_at]
      assert order.save
      assert_operator order.price.fractional, :>, 0
      assert_equal order.deliveries.first.price,
        order.deliveries.first.items.sum { |item| item.price }
      # assert_equal 1 , order.errors
    end
  end

  test "all orders have valid delivery date (not outdated)" do
    assert true
  end
  test "all orders have valid positive number of order_item" do
    assert true
  end

  test "a delivery can have many entries with diff preferences" do
    store = FactoryGirl.create :store_with_boxes
    box =  FactoryGirl.create(:box_with_preferences)
    store.boxes << box
    store.save
    item = FactoryGirl.build :item,
      box_id: box.id,
      quantity: 2,
      preferences: [
        { box_preference: box.preferences.first },
        { box_preference: box.preferences.last }
      ]
    delivery = FactoryGirl.build :delivery,
      items: [ item ],
      delivered_at: Date.today + 3,
      store: store
    params = {
      deliveries: [ delivery ]
    }
    assert_difference 'Delivery.count', +1 do
      order = FactoryGirl.build :order, params
      assert order.save
      assert_equal 1, Order.count
      assert_equal 1, Delivery.count
      assert_equal 2, Delivery.first.items.first.preferences.count
    end
  end

  test "a delivery_entry must has a delivered_at Date" do
    order = FactoryGirl.build :order_with_deliveries
    order.deliveries.first.delivered_at = nil
    assert_difference 'Order.count', +0 do
      assert !order.save
      # assert_equal 1, Order.count
      # assert_equal 1, Delivery.count
      # assert_equal 2, Delivery.first.items.first.preferences.count
    end
    assert_difference 'Order.count', +1 do
      order.deliveries.first.delivered_at = Date.today + 3
      assert order.save
      # assert_equal 1, Order.count
      # assert_equal 2, order.deliveries.first.errors
      # assert_equal 1, Delivery.count
      # assert_equal 2, Delivery.first.items.first.preferences.count
    end
  end

end
