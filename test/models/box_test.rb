require 'test_helper'

class BoxTest < ActiveSupport::TestCase
  test "a box can be created" do
    store = FactoryGirl.build :order_with_deliveries
    box = FactoryGirl.build(:box, store: store)
    assert box.save
    assert_operator box.price.fractional, :>, 0
  end
end
