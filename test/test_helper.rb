ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'json'
require 'timecop'
require 'micro/api_helpers'
require 'database_cleaner'
require "minitest/spec"

DatabaseCleaner.strategy = :truncation

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  extend MiniTest::Spec::DSL

  include Micro::APIHelpers
  include FactoryGirl::Syntax::Methods
  # Add more helper methods to be used by all tests here...
  FactoryGirl.define do
    after(:create) {|obj| obj.class.reindex if obj.class.respond_to?(:reindex) }
  end
  before :each do
    DatabaseCleaner.start
  end
  after :each do
    DatabaseCleaner.clean
  end
end