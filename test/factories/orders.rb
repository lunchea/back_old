FactoryGirl.define do
  factory :box_preference do
    name Faker::Commerce.product_name
  end

  factory :box do
    name Faker::Commerce.product_name
    price 6
    factory :box_with_preferences do
      after(:build) do |box, evaluator|
        box.preferences << FactoryGirl.build(:box_preference, box: box)
      end
    end
  end

  factory :store do
    sequence :email do |n|
      n.to_s + Faker::Internet.email
    end
    verified true
    email_at Time.now - 30.minutes
    delivered_at Time.now
    delivery_address Faker::Address.street_address
    sequence :name do |n|
      Faker::Name.name + n.to_s
    end

    factory :store_with_boxes do
      after(:build) do |store, evaluator|
        store.boxes << FactoryGirl.build(:box, store: store)
      end
    end
  end

  factory :item do
    box
    quantity 3
  end

  factory :delivery do
    delivered_at Faker::Date.forward(23)

    factory :delivery_with_items do
      association :store, factory: :store_with_boxes
      after(:build) do |delivery, evaluator|
        delivery.items << build_list(:item, 3, box: delivery.store.boxes.first )
      end
    end
  end

  factory :order do
    phone "6125944995"
    email Faker::Internet.email

    factory :order_with_deliveries do
      transient do
        deliveries_count 2
      end
      after(:build) do |order, evaluator|
        order.deliveries << build_list(:delivery_with_items, 3, order: order )
      end
    end
  end

end
