# Preview all emails at http://localhost:3000/rails/mailers/meal_mailer
class OrderMailerPreview < ActionMailer::Preview
  # FactoryGirl.create_list :order , 10, delivered_at: Date.today + 1, confirm: true

  def confirmation
    OrderMailer.confirmation(Order.all[-2])
  end

  def cancel
    Order.first
    OrderMailer.cancel(Order.last)
  end

  def summary
    OrderMailer.summary Store.all[0].id, (Date.today + 1.day).to_s
  end

  def expire
    OrderMailer.expire Order.last.id
  end
end
