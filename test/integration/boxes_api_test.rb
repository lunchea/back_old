require 'test_helper'

class BoxsAPITest < ActionDispatch::IntegrationTest
  let(:user) { FactoryGirl.create :user }
  let(:box) { FactoryGirl.create :box }

  test "store account can only crud his lunch boxes" do
  end

  test "admin can crud all lunch boxes" do
  end

  test "anyone can read all lunch boxes" do
  end

  test "user/guest cannot modify lunch boxes" do
    # user
    # hash = pay_load user
    # token = encode_jwt hash
    # put "/api/v1/boxes/#{box.id}", nil, {"X-Jwt" => token}
    # assert_response 401
    # guest
    # put "/api/v1/boxes/#{box.id}"
    # assert_response 401
  end

end