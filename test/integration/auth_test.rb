# require 'test_helper'

# class AuthTest < ActionDispatch::IntegrationTest
#   let(:user) { FactoryGirl.create :user }

#   test "login " do
#     post "/api/v1/auth/login", {email: user.email , password: 'password'}
#     assert_instance_of Array, decode_jwt(@response.header["X-Jwt"])
#   end

#   test "signup " do
#     assert_difference 'User.count', +1 do
#       assert_difference 'ActionMailer::Base.deliveries.size', 1 do
#         post "/api/v1/auth/signup", {email: 'fff@sdfdsf.sf' , password: 'shitting2'}
#         assert_instance_of Array, decode_jwt(@response.header["X-Jwt"])
#       end
#     end
#   end

#   test 'user request reset_password email' do
#     assert_difference 'ActionMailer::Base.deliveries.size', +1 do
#       get '/api/v1/auth/reset?email=' + user.email
#     end
#   end

#   test 'nonuser failed to request reset_password email' do
#     assert_difference 'ActionMailer::Base.deliveries.size', 0 do
#       get '/api/v1/auth/reset?email=sdfaisd@df.csd'
#       assert_response :missing
#     end
#   end

#   test "token helper" do
#     hash = reset_password_token_payload user
#     assert_instance_of User, user
#     token = encode_jwt(hash)
#     payloader = decode_jwt token
#     assert_instance_of Array, payloader, 'token decoding incorrect'
#     _user = user_to_reset_password token
#     assert_instance_of User, _user, 'user not found'
#   end

#   test 'reset password by token' do
#     hash = reset_password_token_payload user
#     token = encode_jwt(hash)
#     new_pwd = "shitd22xaaaa"

#     put '/api/v1/auth/reset', {reset_password_token: token ,password: new_pwd, password_confirmation: new_pwd}
#     assert_response :success
#     user.reload
#     assert user.valid_password?(new_pwd), "new pwd is invalid"
#   end

# end
