require 'test_helper'

class JobsTest < ActiveJob::TestCase
  let(:order) { FactoryGirl.create :order_with_deliveries }
  let(:store) { FactoryGirl.create :store }

  test "OrderingJob can be executed" do
    assert_enqueued_with(job: OrderingJob) do
      OrderingJob.perform_later store.id.to_s
    end
    # OrderingJob.drain

    # assert_difference 'ActionMailer::Base.deliveries.size', +1  do
    #   OrderingJob.perform_now store.id.to_s
    #   # OrderingJob.drain
    # end
  end

  test "ExpireJob can be executed" do
    assert_enqueued_with(job: ExpireJob)  do #a job is created to send all the expiry emails tmr
      ExpireJob.perform_later order.id.to_s
    end
  end

  test "PartnerJob can be executed" do
    assert_enqueued_with(job: PartnerJob) do
      PartnerJob.perform_later "sbuforum"
    end

    # assert_difference 'ActionMailer::Base.deliveries.size', +1  do
    #   PartnerJob.perform_now "sbuforum"
    # end
  end

  test "OrderingJob runs on time" do
    # assert_difference 'OrderingJob.jobs.size', +1  do
    #   Timecop.scale(3600*6)
    #   sleep 4
    # end
  end
end
