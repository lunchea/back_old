require 'test_helper'

class OrdersAPITest < ActionDispatch::IntegrationTest
  def date_of_next_weekday(date)
    if (date.saturday?)
      date += 1  #if it's a saturday, change it to a sunday, as the next day will be monday
    end
    if (date.friday?)
      date += 2  #if it's a friday, change it to a sunday, as the next day will be monday
    end
    date + 1
  end

  # before :each do
  #   Order.reindex
  # end

  let(:user) { FactoryGirl.create :user }
  let(:store) { FactoryGirl.create :store_with_boxes }
  let(:order) { FactoryGirl.create :order }
  let(:delivery) { FactoryGirl.create :delivery }
  let(:user_order) { FactoryGirl.create :order_with_deliveries }
  # let :params do
  #   {
  #     phone: "6122054615",
  #     email: "hardy0wu@gmail.com",
  #     deliveries_attributes: [
  #       {
  #         delivered_at: date_of_next_weekday(Date.today),
  #         items_attributes: [
  #           {
  #             box: {
  #               price: 6,
  #               name: "bentoBox"
  #             },
  #             quantity: 2
  #           }
  #         ]
  #       }
  #     ]
  #   }
  # end
  let :params do
    {
      deliveries_attributes: [
        {
          delivered_at: date_of_next_weekday(Date.today),
          items_attributes: [
            {
              quantity: 1
            }
          ]
        }
      ],
      email: 'hardy0wu@gmail.com',
      phone: '6122054615'
    }
  end

  test "anyone can create a order" do
    params[:deliveries_attributes].each do |deli|
      deli[:store_id] = store.id.to_s
      deli[:items_attributes].each {|x| x[:box_id] = store.boxes.first.id.to_s }
    end

    assert_difference 'Order.count', +1 do
      # assert_difference 'ActionMailer::Base.deliveries.size', 1 do #doesn't work with deliver_later
        post "/api/v1/orders", params
        assert_response :success
        assert_instance_of Hash, JSON.parse(@response.body)
        # assert_equal params[:deliveries].delivered_at, Order.last.delivered_at
      # end
    end
  end

  test "no one can create late orders" do
    params[:deliveries_attributes].each do |deli|
      deli[:store_id] = store.id.to_s
      deli[:items_attributes].each {|x| x[:box_id] = store.boxes.first.id.to_s }
      deli[:delivered_at] = Date.today - 1
    end

    assert_difference 'Order.count', 0 do
      post "/api/v1/orders", params
      assert_response 422
      assert_instance_of Hash, JSON.parse(@response.body)
    end
  end

  test "anyone can count deliveries of a store at particular day" do
    # params = {
    #   delivered_at: date_of_next_weekday(Date.today)
    # }

    # get "/api/v1/orders/count", params
    # assert_response :success
    # size = @response['X-total']
    # sizecount = size.to_i
    # assert_equal Order.where(delivered_at: params[:delivered_at], confirm: true).count, sizecount
    # assert_difference 'sizecount', +0 do
    #   order = FactoryGirl.build :order_with_deliveries
    #   order.deliveries.each do |deli|
    #     deli.delivered_at = date_of_next_weekday(Date.today)
    #   end
    #   order.save
    #   assert Order.find(order.id)
    #   get "/api/v1/orders/count", params
    #   assert_response :success
    #   size = @response['X-total']
    #   sizecount = size.to_i
    # end
  end

  test "store account can get all orders for him" do
  end

  test "admin can crud all the orders" do
  end

  test 'user can get all his past orders' do
    user_order.update user_id: user.id, email: user.email
    hash = pay_load user
    get "/api/v1/orders", nil, {"X-Jwt" => encode_jwt(hash) }
    assert_instance_of Array, JSON.parse(@response.body)
    assert_equal user.id.to_s, JSON.parse(@response.body)[0]['user_id']
  end

  test 'guest can only get empty order list' do
    user_order.update user_id: user.id, email: user.email
    get "/api/v1/orders"
    assert [], @response.body
  end

  test "user can confirm order by access token, and cancel order, with confirmation email" do
    order = FactoryGirl.build :order_with_deliveries
    order.confirmed_at = nil
    order.save
    assert Order.find(order.id)
    params = {
      access_token: order.access_token
    }
    assert_difference 'Order.count', 0 do
      assert_difference 'Order.confirmed.count', 1 do
        assert_difference 'Order.unconfirmed.count', -1 do
          put "/api/v1/orders/#{order.id}/confirm", params
          assert_response :success
          assert_not_nil JSON.parse(@response.body)['confirmed_at']
          timeDiff = DateTime.now.to_f - Time.parse(JSON.parse(@response.body)['confirmed_at']).to_f
          assert_operator 1, :>, timeDiff.abs #confirmation time and present time differ by less than 1 second
        end
      end
    end
    assert_difference 'Order.count', 0 do
      assert_difference 'Order.confirmed.count', -1 do
        assert_difference 'Order.unconfirmed.count', 1 do
          # assert_difference 'ActionMailer::Base.deliveries.size', 1 do
            delete "/api/v1/orders/#{order.id}/cancel", params
            assert_response :success
          # end
        end
      end
    end
  end

  test "order delivery must have at least one item" do
    params[:deliveries_attributes].each do |deli|
      deli[:store_id] = store.id.to_s
      deli[:items_attributes] = nil
      deli[:delivered_at] = Date.today + 1
    end

    assert_difference 'Order.count', 0 do
      post "/api/v1/orders", params
      assert_response 422
    end
  end

  test "order must have at least one delivery" do
    params[:deliveries_attributes] = nil

    assert_difference 'Order.count', 0 do
      post "/api/v1/orders", params
      assert_response 422
    end
  end
end
