require 'test_helper'

class UserAPITest < ActionDispatch::IntegrationTest
  let(:user) { FactoryGirl.create :user }

  test "get users" do
    get "/api/v1/users", :format => :json
    assert_instance_of Array, JSON.parse(@response.body)
  end

  # test "search users by username" do
  #   get "/api/v1/users?username=css", :format => :json
  #   assert_instance_of Array, JSON.parse(@response.body)
  # end
end
