require 'test_helper'

class StoresAPITest < ActionDispatch::IntegrationTest
  let(:store) { FactoryGirl.create :store }

  test "get stores" do
    get "/api/v1/stores", :format => :json
    assert_response :success
    assert_instance_of Array, JSON.parse(@response.body)
  end

  # test "search users by username" do
  #   get "/api/v1/users?username=css", :format => :json
  #   assert_instance_of Array, JSON.parse(@response.body)
  # end

end
