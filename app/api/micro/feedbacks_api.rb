module Micro
  class FeedbacksAPI < Grape::API
    module Config
      extend Grape::API::Helpers

      MODEL_NAME = 'feedbacks'

      def _model_name
        'Feedback'
      end

      params :parent do
        optional :title, type: String
        optional :content, type: String
        optional :user_id
        optional :store_id
      end

      params :getlist do
        optional :title, type: String
        optional :content, type: String
        optional :user_id
        optional :store_id
      end
    end

    include Micro::Concerns::Restful
  end
end
