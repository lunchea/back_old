module Micro
  class OrdersAPI < Grape::API
    module Config
      extend Grape::API::Helpers

      MODEL_NAME = 'orders'

      def _model_name
        'Order'
      end

      params :parent do
        optional :phone, type: String
        optional :email
        optional :access_token
        optional :source
        optional :deliveries_attributes, type: Array do
          optional :delivered_at, type: Date
          optional :store_id
          optional :items_attributes, type: Array do
            optional :box_id
            optional :quantity
            optional :preferences_attributes, type: Array do
              optional :box_preference_id
              optional :value
            end
          end
        end
      end

      params :getlist do
        optional :phone, type: String
        optional :email
        # optional :source
      end

    end

    helpers self::Config
    params do
      use :parent
    end
    resource :orders do

      post do
        @options = declared params, include_missing: false
        order = Order.new @options
        error! 'Unauthorized.', 401 unless
          OrderPolicy.new(current_user, order).create?
        if order.save
          order.send_confirmation
          present order, with: Entities::Order
        else
          present order, with: Entities::Order
          status 422
        end
      end


      put ':id/confirm' do
        order = Order.find(params[:id])
        error! 'Unauthorized.', 401 unless
          params[:access_token] == order.access_token or
          OrderPolicy.new(current_user, order).update?
        if order.confirm!
          present order, with: Entities::Order
        else
          present order, with: Entities::Order
          status 422
        end
      end

      delete ":id/cancel" do
        order =  Order.find(params[:id])
        error! 'Unauthorized.', 401 unless
          params[:access_token] == order.access_token or
          OrderPolicy.new(current_user, order).destroy?
        if order.cancel!
          present order, with: Entities::Order
          status 204
        else
          present order, with: Entities::Order
          status 422
        end
      end
    end

    include Micro::Concerns::Restful

  end
end
