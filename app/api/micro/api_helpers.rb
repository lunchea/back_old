module Micro
  module APIHelpers
    def encode_jwt(hash)
      secret = Rails.application.secrets['secret_key_base']
      JWT.encode(hash, secret, "HS384")
    end

    def decode_jwt(token)
      secret = Rails.application.secrets['secret_key_base']
      JWT.decode(token, secret, "HS384")
    end

    def current_user
      # return @current_user if @current_user # implement backend logout to set this to nil
      if headers['X-Jwt']
        payloader = decode_jwt headers['X-Jwt']
        # @current_user = User.find payloader[0]["id"]
        return User.find payloader[0]["id"]
      else
        return false
      end
    rescue JWT::DecodeError
      false
    end

    def authenticate!
      error!('Unauthorized.', 401) unless current_user
    end

    def pay_load(user)
      {
        id: user.id.to_s,
        email: user.email,
        exp: (Time.zone.now + 1.week).to_i
      }
    end

    def reset_password_token_payload(user)
      {
        id: user.id.to_s,
        email: user.email,
        type: 'reset_password_token',
        exp: Time.zone.now + 1.day
      }
    end

    def user_to_reset_password(token)
      return @user_to_reset_password if @user_to_reset_password
      if token
        payloader = decode_jwt token
        @user_to_reset_password = User.find payloader[0]["id"]
      else
        false
      end
    rescue JWT::DecodeError
      false
    end

    def confirm_email_token_payload(user)
      {
        id: user.id.to_s,
        email: user.email,
        type: 'confirmation_token',
        exp: Time.zone.now + 1.day
      }
    end

    def user_to_confirm_email(token)
      return @user_to_confirm_email if @user_to_confirm_email
      if token
        payloader = decode_jwt token
        @user_to_confirm_email = User.find payloader[0]["id"]
      else
        false
      end
    rescue JWT::DecodeError
      false
    end

    def locale
      current_user.try(:locale) ||
      http_accept_language \
      .compatible_language_from(I18n.available_locales) ||
      I18n.default_locale
    end

    def x_headers_of_elasticsearch(instances)
      header 'X-Total', instances.total_count
      header 'X-Page-Total', instances.total_pages
      header 'X-Page', instances.current_page
      header 'X-Per-Page', instances.per_page
    end
  end
end