module Micro
  class AuthAPI < Grape::API
    desc 'Authenticate: login, signup, reset'
    resource :auth do
      params do
        requires :email, type: String, desc: "Email"
        requires :password, type: String, desc: "Password"
      end
      post 'login' do
        user = User.find_by email: params[:email]
        if user && user.valid_password?(params[:password])
          hash = pay_load user
          token = encode_jwt hash
          header 'X-Jwt', token
          present user, with: Entities::User, token: token
        else
          present user, with: Entities::User
          status 401
        end
      end
      params do
        requires :email, type: String, desc: "Email"
        requires :password, type: String, desc: "Password"
      end
      post 'signup' do
        user = User.new email: params[:email],
                        password: params[:password]
        if user.save
          hash_email = confirm_email_token_payload user
          token = encode_jwt(hash_email)
          UserMailer.confirmation_instructions(user.id.to_s, token).deliver_later
          hash = pay_load user
          token = encode_jwt hash
          header 'X-Jwt', token
          present user, with: Entities::User, token: token
        else
          present user, with: Entities::User
          status 401
        end
      end
      params do
        requires :email, type: String, desc: "Email"
      end
      get 'reset' do
        user = User.find_by email: params[:email]
        error!('User not found.', 404) unless user and user.valid?
        hash = reset_password_token_payload user
        token = encode_jwt(hash)
        UserMailer.send_reset_password_instructions(user.id.to_s, token).deliver_later
      end
      params do
        requires :reset_password_token, type: String, desc: "token"
        requires :password, type: String, desc: "Password"
        requires :password_confirmation, type: String, desc: "Password"
      end
      put 'reset' do
        user = user_to_reset_password params[:reset_password_token]
        error!('User not found.', 404) unless user and user.valid?
        if !user.reset_password! params[:password],
                                 params[:password_confirmation]
          error!('Internal Server Error.', 500)
        end
      end
      params do
        requires :confirmation_token, type: String, desc: "token"
      end
      put 'confirm' do
        user = user_to_confirm_email params[:confirmation_token]
        error!('User not found.', 404) unless user and user.valid?
        if user.confirm!
          hash = pay_load user
          token = encode_jwt hash
          header 'X-Jwt', token
          present user, with: Entities::User, token: token
        else
          status 422
        end
      end

    end
  end
end
