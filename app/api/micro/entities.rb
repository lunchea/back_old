module Micro
  module Entities
    # class User < Grape::Entity
    #   expose :id, :username, :email, :fake, :gravatar_url
    # end
    class Base < Grape::Entity
      expose :id do |model|
        model.id.to_s
      end
      expose :errors
      expose :full_messages do |record|
        record.errors.full_messages
      end
    end


    class Profile < Base
      expose :phone
      expose :username
    end

    class User < Base
      expose :email
      expose :profile, with: Profile
      expose :token, if: :token do |instance, options|
        options[:token]
      end
    end

    class BoxPreference < Base
      expose :name, :category, :type
    end

    class Box < Base
      expose :name, :chinese_name, :category, :description,
        :started_at, :expired_at, :max_preferences_number
      expose :store_id do |model|
        model.store_id.to_s
      end
      # expose :price do |box|
      #   box.try(:price).try :format_with_settings
      # end
      expose :price do
        expose :fractional do |box|
          box.try(:price).try :fractional
        end
        # expose :currency do |box|
        #   box.try(:price).try :currency
        # end
      end
      expose :preferences, with: BoxPreference
    end

    class ItemPreference < Base
      expose :value
    end

    class Item < Base
      expose :quantity
      expose :box_id do |model|
        model.box_id.to_s
      end
      expose :box, with: Box
      expose :preferences, with: ItemPreference
      expose :box_name do |item|
        item.box.try :name
      end
      expose :box_price do |item|
        item.box.try(:price).try :format_with_settings
      end
    end

    class Delivery < Base
      expose :order_id do |order|
        order.order_id.to_s
      end
      expose :store_id do |order|
        order.store_id.to_s
      end
      expose :delivered_at, :confirmed_at, :phone, :email, :errors
      expose :items, using: Item
      expose :price
    end

    class Order < Base
      expose :user_id do |order|
        order.user_id.to_s
      end
      expose :phone, :email, :errors, :confirmed_at
      expose :deliveries, using: Delivery
      expose :access_token, if: {include_access_token: true}
      expose :source
      expose :discount do |order|
        order.try(:discount).try(:fractional)
      end
      expose :price do |order|
        order.try(:price).try :format_with_settings
      end
    end

    class Feedback < Base
      expose :user_id do |feedback|
        feedback.user_id.to_s
      end
      expose :title, :content
      expose :store_id do |feedback|
        feedback.store_id.to_s
      end
    end

    class Store < Base
      expose :boxes, with: Box
      expose :name, :delivered_at, :delivery_address, :email_at,
              :discount_max_count, :map_address, :email,
              :confirmed_at, :min_delivery_sale
      expose :discount_count do |store|
        store.discount_count
      end
      expose :has_discount do |store|
        store.discount?
      end
    end
  end
end
