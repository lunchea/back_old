module Micro
  class StoresAPI < Grape::API
    module Config
      extend Grape::API::Helpers

      MODEL_NAME = 'stores'

      def _model_name
        'Store'
      end

      params :parent do
        # optional :admin_id
        optional :name, type: String
        optional :email, type: String
        optional :email_at, type: String
        optional :verified, type: Boolean
        optional :delivered_at, type: Time
        optional :delivery_address
        optional :min_delivery_sale
      end

      params :getlist do
        # optional :admin_id
        optional :name, type: String
        optional :confirmed_at
        optional :email
      end
    end
    resource :stores do
      params do
        requires :date, type: Date
      end
      get ':id/sale' do
        store = Store.find(params[:id])
        error! 'Unauthorized.', 401 unless
          StorePolicy.new(current_user, store).show?
        store.sale_of params[:date]
      end

      params do
        optional :name, type: String
        optional :confirmed_at
        optional :email
      end
      get 'owned' do
        options = declared params,
                            include_missing: false,
                            include_parent_namespaces: false

        scope = StorePolicy::Scope.new(current_user, Store).owned
        instances = scope.where(options).
                    page(params[:page]).
                    per(params[:per_page])

        header 'X-Total', instances.total_count
        header 'X-Page-Total', instances.total_pages
        header 'X-Page', instances.current_page
        present instances, with: Entities::Store
      end

      put ':id/confirm' do
        store = Store.find(params[:id])
        error! 'Unauthorized.', 401 unless
          StorePolicy.new(current_user, store).confirm?
        if store.confirm!
          present store, with: Entities::Store
        else
          present store, with: Entities::Store
          status 422
        end
      end

      delete ":id/cancel" do
        store =  Store.find(params[:id])
        error! 'Unauthorized.', 401 unless
          StorePolicy.new(current_user, store).destroy?
        if store.cancel!
          present store, with: Entities::Store
          status 204
        else
          present store, with: Entities::Store
          status 422
        end
      end
    end
    include Micro::Concerns::Restful
  end
end
