module Micro
  class API < Grape::API
    version 'v1', using: :path
    format :json
    default_format :json
    rescue_from Mongoid::Errors::DocumentNotFound do |e|
      hash = { errors: [ e.message ] }
      rack_response hash.to_json, 404
    end
    rescue_from :all

    helpers APIHelpers
    mount Micro::AuthAPI
    mount Micro::UsersAPI
    mount Micro::BoxesAPI
    mount Micro::OrdersAPI
    mount Micro::StoresAPI
    mount Micro::DeliveriesAPI
    add_swagger_documentation api_version: "v1", base_path: "api"
  end
end
