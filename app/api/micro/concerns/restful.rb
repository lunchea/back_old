module Micro
  module Concerns
    module Restful
      # if you're using Grape outside of Rails, you'll have to use Module#included hook
      extend ActiveSupport::Concern

      included do
        helpers self::Config
        helpers do
          def _class
            _model_name.constantize
          end

          def _entity
            Entities.const_get(_model_name)
          end

          def _policy
            (_model_name + 'Policy').constantize
          end
        end

        params do
          use :parent
        end
        resource self::Config::MODEL_NAME do
          after_validation do
            @options = declared params, include_missing: false
          end
          params do
            use :getlist
          end
          get do
            @options = declared params,
                                include_missing: false,
                                include_parent_namespaces: false

            scope = _policy::Scope.new(current_user, _class).resolve
            instances = scope.where(@options).
                        page(params[:page]).
                        per(params[:per_page])

            header 'X-Total', instances.total_count
            header 'X-Page-Total', instances.total_pages
            header 'X-Page', instances.current_page
            present instances, with: _entity
          end

          params do
            use :getlist
          end
          get "count" do
            @options = declared params,
                                include_missing: false,
                                include_parent_namespaces: false

            scope = _class # allow anyone to get count
            instances_count = scope.where(@options).count

            header 'X-Total', instances_count
          end

          params do
            use :getlist
          end
          get "count_confirmed" do
            @options = declared params,
                                include_missing: false,
                                include_parent_namespaces: false

            scope = _class # allow anyone to get count_confirmed
            instances_count = scope.try(:confirmed).try(:where, @options).try(:count)

            header 'X-Total', instances_count
          end

          get ':id' do
            instance =  _class.find(params[:id])
            error! 'Unauthorized.', 401 unless
              _policy.new(current_user, instance).show?
            present instance, with: _entity
          end

          post do
            instance = _class.new @options
            error! 'Unauthorized.', 401 unless
              _policy.new(current_user, instance).create?
            if instance.save
              present instance, with: _entity
            else
              present instance, with: _entity
              status 422
            end
          end

          put ':id' do
            instance =  _class.find(params[:id])
            error! 'Unauthorized.', 401 unless
              _policy.new(current_user, instance).update?
            if instance.update @options
              present instance, with: _entity
            else
              present instance, with: _entity
              error! 422
            end
          end

          delete ':id' do
            instance = _class.find(params[:id])
            error! 'Unauthorized.', 401 unless
              _policy.new(current_user, instance).destroy?
            if instance.destroy
              status 204
            else
              present instance, with: _entity
              status 401
            end
          end
        end
      end
    end
  end
end