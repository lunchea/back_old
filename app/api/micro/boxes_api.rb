module Micro
  class BoxesAPI < Grape::API
    module Config
      extend Grape::API::Helpers

      MODEL_NAME = 'boxes'

      def _model_name
        'Box'
      end

      params :parent do
        optional :name, type: String
        optional :chinese_name, type: String
        optional :price
        optional :store_id
        optional :category
        optional :max_preferences_number, type: Integer
        optional :started_at, type: Date
        optional :expired_at, type: Date
        optional :preferences_attributes, type: Array do
          optional :id
          optional :_destroy
          optional :name
          optional :type
        end
      end

      params :getlist do
        optional :name, type: String
        optional :chinese_name, type: String
        optional :price
        optional :store_id
        # optional :started_at, type: Date
        # optional :expired_at, type: Date
        optional :delivered_at, type: Date
      end
    end
    resource :boxes do
      helpers self::Config
      params do
        use :getlist
      end
      get do
        options = declared params,
                  include_missing: false,
                  include_parent_namespaces: false,
                  ignore_unmapped: true

        if options[:delivered_at]
          options[:started_at] = {lt: options[:delivered_at]}
          options[:expired_at] = {gt: options[:delivered_at]}
          options.delete :delivered_at
        end

        options.merge! BoxPolicy::Scope.new(current_user, Box).options

        boxes = Box.search params[:query] || "*",
                        where: options,
                        page: params[:page],
                        per_page: params[:per_page] || Box.default_per_page,
                        order: { category: :desc }

        x_headers_of_elasticsearch(boxes)
        present boxes, with: Entities::Box
      end

    end
    include Micro::Concerns::Restful
  end
end
