module Micro
  class DeliveriesAPI < Grape::API
    module Config
      extend Grape::API::Helpers

      MODEL_NAME = 'deliveries'

      def _model_name
        'Delivery'
      end

      params :parent do
        optional :order_id
        optional :delivered_at, type: Date
        optional :store_id
        optional :items, type: Array do
          optional :box_id
          optional :quantity
          optional :preferences, type: Array do
            optional :box_preference_id
            optional :value
          end
        end
      end

      params :getlist do
        optional :order_id
        optional :delivered_at, type: Date
        optional :store_id
      end
    end

    include Micro::Concerns::Restful
  end
end
