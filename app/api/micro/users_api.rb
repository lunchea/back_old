module Micro
  class UsersAPI < Grape::API
    resource :users do
#      get ":username" do
#        user = User.find_by username: params[:username]
#        user ||= User.find params[:username]
#        present user, with: Entities::User
#      end
    end

    module Config
      extend Grape::API::Helpers

      MODEL_NAME = 'users'

      def _model_name
        'User'
      end

      params :parent do
        optional :email #requires :email, :type => String, :desc => "Email"
        optional :password #requires :password, :type => String, :desc => "Password"
        optional :profile, type: Hash do
          optional :phone
          optional :username
        end
      end

      params :getlist do
#        optional :username
        optional :email
      end

    end

    include Micro::Concerns::Restful
  end
end