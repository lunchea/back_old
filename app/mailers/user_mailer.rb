class UserMailer < ActionMailer::Base
  default from: "account@scixiv.com"

  def send_reset_password_instructions(user_id, token)
    @resource = User.find user_id
    @token = token
    mail(to: @resource.email, subject: "Lunchea.com - Reset password instructions" ) do |format|
      format.html
    end
  end

  def confirmation_instructions(user_id, token)
    @resource = User.find user_id
    @token = token
    mail(to: @resource.email, subject: "Lunchea.com - Please confirm your email" ) do |format|
      format.html
    end
  end
end
