class OrderMailer < ActionMailer::Base
  default from: "orders@lunchea.com"
  holidays = ['11/27/14','11/28/14','12/24/14','12/25/14','12/31/14',
                '1/1/15', '1/26/15', '1/27/15', '1/28/15']
  @@holidays1 = holidays.map { |date| Date.strptime(date, "%m/%d/%y")}

  def confirmation(order_id)
    if @order = Order.find(order_id)
      item_name = @order.deliveries.first.items.first.box.name
      mail(to: @order.email, subject: (@order.confirmed? ? 'Y' : 'Please confirm y') \
       + 'our Lunchea order of "' \
       + item_name + '"' + (@order.items.count > 1 ? ' and more...' : "" ) ) do |format|
        format.html
      end
    end
  end

  def cancel(order_id)
    if @order = Order.find(order_id)
      item_name = @order.deliveries.first.items.first.box.name
      mail(to: @order.email, subject: 'Your Lunchea order of "' \
        + item_name + '"' + (@order.items.count > 1 ? ' and more...' : "" ) + ' has been cancelled' ) do |format|
        format.html
      end
    end
  end

  def summary(store_id, delivered_at)
    delivered_at = Date.parse(delivered_at)
    if (!(@@holidays1.include? delivered_at))
      @store = Store.find store_id
      @deliveries = @store.deliveries.confirmed.where delivered_at: delivered_at
      mail(to: @store.email, cc: 'orders@lunchea.com', subject: "Orders from Lunchea" ) do |format|
        format.html
      end
    end
  end

  def summary_test(store_id, delivered_at, email_to)
    delivered_at = Date.parse(delivered_at)
    if (!(@@holidays1.include? delivered_at))
      @store = Store.find store_id
      @deliveries = @store.deliveries.confirmed.where delivered_at: delivered_at
      mail(to: email_to,
        subject: "Orders from Lunchea",
        template_name: "summary")
    end
  end

  def partner_summary(source_id, delivered_at)
    delivered_at = Date.parse(delivered_at)
    if (!(@@holidays1.include? delivered_at))
      @orders = Order.confirmed.where source: source_id
      @deliveries = Delivery.in(order_id: @orders.map(&:id)).
                      where(delivered_at: delivered_at)
      mail(to: 'support@sbuforum.com', cc: 'orders@lunchea.com', subject: "Order Summary from Lunchea" ) do |format|
        format.html
      end
    end
  end

  def partner_summary_test(source_id, delivered_at, email_to)
    delivered_at = Date.parse(delivered_at)
    if (!(@@holidays1.include? delivered_at))
      @orders = Order.confirmed.where source: source_id
      @deliveries = Delivery.in(order_id: @orders.map(&:id)).
                      where(delivered_at: delivered_at)
      mail(to: email_to,
        subject: "Order Summary from Lunchea",
        template_name: "partner_summary")
    end
  end

  def expire(order_id)
    if @order = Order.find(order_id)
      item_name = @order.deliveries.first.items.first.box.name
      mail(to: @order.email, subject: 'Your Lunchea order of "' \
        + item_name + '"' + (@order.items.count > 1 ? ' and more...' : "" ) + ' has expired' ) do |format|
        format.html
      end
    end
  end


  def processed delivered_at
    delivered_at = Date.parse(delivered_at)
    @orders = Order.confirmed.where delivered_at: delivered_at
    @emails = @orders.map {|order| order.email}
    if @orders.count > 0
      @orders.each do |order|
        mail(to: order.email, subject: "Your Lunch Boxes have been processed" ) do |format|
          format.html
        end
      end
    end
  end
end
