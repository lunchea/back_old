class UserPolicy < ApplicationPolicy
  def show?
    return false if !user.is_a? User
    record.id == user.id or user.has_role? :admin
  end

  def create?
    true
  end

  def update?
    return false if !user.is_a? User
    record.id == user.id or user.has_role? :admin
  end

  class Scope < Scope
    def resolve
      if !user.is_a? User
        scope.none
      elsif user.has_role? :admin
        scope
      else
        scope.where id: user.id
      end
    end
  end
end
