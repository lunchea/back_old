class StorePolicy < ApplicationPolicy
  def create?
    if user.is_a? User
      user.add_role :store_admin, record
    end
  end

  def update?
    user.is_a? User and (user.has_role? :admin or user.has_role? :store_admin, record)
  end

  def confirm?
    user.is_a? User and user.has_role? :admin
  end

  def destroy?
    user.is_a? User and user.has_role? :admin
  end

  class Scope < Scope
    def resolve
      scope.confirmed.order(created_at: :desc)
    end
    def owned
      if user.is_a? User 
        if user.has_role? :admin
          scope.order(created_at: :desc)
        elsif user.has_role? :store_admin, :any
          scope.confirmed.any_in(_id: scope.with_role(:store_admin, user).collect(&:id)).order(created_at: :desc)
        else
          scope.none
        end
      end
    end
  end
end
