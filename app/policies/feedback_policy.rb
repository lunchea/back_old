class FeedbackPolicy < ApplicationPolicy
  def update?
    record.user_id == user.id
  end

  def create?
    user.is_a?(User) and user.persisted?
  end

  def destroy?
    update?
  end

  class Scope < Scope
    def resolve
      scope.or(user: user, :store.in => user.store_ids)
    end
  end
end
