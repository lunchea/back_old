class OrderPolicy < ApplicationPolicy
  def create?
    if user.is_a? User
      record.user_id = user.id
      if record.email==user.email
        if user.profile.phone.blank? #update user phone number if not previously given
          user.profile.phone = record.phone
          if user.profile.save
            # saved phone number successfully
          else
            # didn't save phone number succesfully
          end
        end
        if user.confirmed? #if user has confirmed oneself, automatically confirm his order
          record.confirmed_at = Time.zone.now #dependent on branch correct_confirmation_erb
        end
      end
    end
    true
  end

  def update?
    true
  end

  def show?
    true
  end

  def destroy?
    return false unless user.is_a? User
    user.has_role? :admin
  end

  class Scope < Scope
    def resolve
      if !user.is_a? User
        scope.none
      elsif user.has_role? :admin
        scope.order(created_at: :desc)
      else
        scope.order(created_at: :desc).where user_id: user.id
      end
    end
  end
end
