class Feedback
  include Mongoid::Document

  belongs_to :user
  belongs_to :store

  field :title, type: String
  field :content, type: String
end
