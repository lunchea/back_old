module Confirmable
  # if you're using Grape outside of Rails, you'll have to use Module#included hook
  extend ActiveSupport::Concern

  included do
    field :confirmed_at, type: Time, default: nil
    scope :confirmed, -> {ne( confirmed_at: nil)}
    scope :unconfirmed, -> {where( confirmed_at: nil)}

    def confirm!
      if !self.confirmed?
        self.update confirmed_at: Time.zone.now
      else
        true #prevent user from confirming multiple times
      end
    end
    def cancel!
      if self.confirmed?
        self.update confirmed_at: nil
      else
        true #prevent user from confirming multiple times
      end
    end
    def confirmed?
      self.confirmed_at != nil
    end
  end
end