class Box
  include Mongoid::Document
  include Mongoid::Timestamps
  searchkick index_name: "box"

  field :name, type: String
  field :chinese_name, type: String
  field :category, type: String
  field :description, type: String
  field :desciption, type: String
  field :price, type: Money
  field :started_at, type: Date
  field :expired_at, type: Date
  field :max_preferences_number, type: Integer

  belongs_to :store
  has_many :items
  has_many :preferences, class_name: "BoxPreference"
  accepts_nested_attributes_for :preferences, allow_destroy: true

  scope :delivered_at, ->(date) {where(:started_at.lte => date,  :expired_at.gte => date)}
  paginates_per 30

  def search_data
    {
      name: name,
      chinese_name: chinese_name,
      category: category,
      description: description,
      price: price,
      started_at: started_at,
      expired_at: expired_at,
      store_id: store_id.to_s,
      preferences: preferences,
      created_at: created_at
    }
  end
end
