class Store
  include Mongoid::Document
  include Mongoid::Timestamps
  include Confirmable
  resourcify

  field :name, type: String
  field :email, type: String
  field :email_at, type: Time
  field :delivery_address, type: String
  field :delivered_at, type: Time
  field :verified, type: Boolean
  field :delivers_weekend, type: Boolean
  field :discount_started_at, type: Date
  field :discount_expired_at, type: Date
  field :discount_max_count, type: Integer
  field :discount_amount, type: Money
  field :map_address, type: String
  field :min_delivery_sale, type: Money, default: 0

  belongs_to :store_admin, class_name: 'User'
  has_many :boxes, autosave: true
  has_many :deliveries
  has_many :feedbacks

  validates :delivered_at, presence: true
  validates :delivery_address, presence: true
  validates :name, presence: true
  validates :name, uniqueness: {scope: :delivered_at,
              message: "Each store + delivered_at combination should be unique."}

  def send_summary(delivered_at)
    OrderMailer.summary(self.id.to_s, delivered_at.to_s).deliver_later
  end

  def time_of(day, default_time, try_hour, try_minute)
    Time.zone.local  \
      day.year,
      day.month,
      day.day,
      default_time.try(:hour) || try_hour,
      default_time.try(:min) || try_minute,
      default_time.try(:sec) || 0
  end

  def delivery_time_of(day)
    time_of(day, delivered_at, 12 - Time.now.utc_offset/3600, 40)
  end

  def email_time_of(day)
    time_of(day, email_at, 9 - Time.now.utc_offset/3600, 0)
  end

  def next_time(next_time)
    if Time.zone.now >= next_time
      next_time += 1.day
    end
    if !delivers_weekend
      if next_time.saturday?
        next_time += 2.day
      elsif next_time.sunday?
        next_time += 1.day
      end
    end
    next_time
  end

  def next_delivery_time
    next_time(delivery_time_of(Date.today))
  end

  def next_email_time
    next_time(email_time_of(Date.today))
  end

  def within_discount_period(date)
    if (discount_started_at)
      if (date >= discount_started_at)
        if (discount_expired_at)
          if (date <= discount_expired_at)
            return true
          end
        end
        return true
      end
    end
    false
  end

  def within_discount_period?
    within_discount_period(Date.today)
  end

  def discount?
    within_discount_period? && (discount_count < (self.discount_max_count || 0))
  end

  def discount_count
    discount_deliveries.try(:count) || 0
  end

  def discount_deliveries
    if within_discount_period?
      self.deliveries.confirmed.where delivered_at: {:$gte => self.discount_started_at}
    end
  end

  def sale_of(date)
    Delivery.confirmed.where(delivered_at: date, store_id: self.id).map(&:price).reduce(:+)
  end
end
