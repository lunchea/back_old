class Delivery
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Alize


  field :delivered_at, type: Date

  belongs_to :order, inverse_of: :deliveries
  belongs_to :store, inverse_of: :deliveries
  has_many :items, autosave: true

  delegate :email, to: :order, allow_nil: true
  delegate :phone, to: :order, allow_nil: true
  alize :order, :confirmed_at

  validates :items, presence: true
  validates :items, associated: true
  validates :store, presence: true
  validates :store, associated: true
  validates :delivered_at, presence: true
  validates :order, presence: true
  validates :order, associated: true
  validate :check_time
  validate :check_store_id
  accepts_nested_attributes_for :items

  scope :confirmed, -> {ne( "order_fields.confirmed_at" => nil)}
  scope :unconfirmed, -> {where( "order_fields.confirmed_at" => nil)}

  def confirmed_at
    has_confirmed_at = self.order_fields['confirmed_at'].nil? rescue true
    unless has_confirmed_at
      self.order_fields['confirmed_at']
    end
  end

  def confirmed?
    self.confirmed_at != nil
  end

  def check_store_id
    ids = items.map(&:box).map(&:store_id).map(&:to_s).uniq
    if ids.size != 1 or ids[0] != store_id.to_s
      errors.add(:store_id, ids.inspect)
    end
  end

  def email_time
    self.store.email_time_of self.delivered_at
  end

  def check_time
    if !delivered_at.is_a? Date
      return false
    end
    errors.add(:delivered_at, "cannot accept late orders") unless
      Time.zone.now < email_time
  end

  def total_boxes
    self.items.map(&:quantity).reduce(:+)
  end

  def price
    self.items.sum { |p| p.price || 0 }
  end

  def discount?
    self.store and self.store.within_discount_period(self.delivered_at) &&
      self.discount_number <= self.store.discount_max_count
  end

  def discount_number
    if self.store and self.store.within_discount_period(self.delivered_at)
      self.store.discount_deliveries.where(_id: { :$lt => self.id } ).count + 1
    end
  end
end
