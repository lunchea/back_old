class BoxPreference
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :type, type: String
  field :category, type: String

  belongs_to :box, inverse_of: :preferences

end
