class ItemPreference
  include Mongoid::Document
  include Mongoid::Timestamps
  field :value, type: String

  belongs_to :box_preference, class_name: "BoxPreference"

  embedded_in :item, inverse_of: :preferences

  validate :check_box_preference

  def check_box_preference
    if self.item.box.preferences.count > 0
      :box_preference_id != nil
    else
      true
    end
  end

  def display_value
    if self.value
      self.value
    else
      'N/A'
    end
  end
end
