class Order
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Alize
  include Confirmable

  field :phone, type: String
  field :access_token, type: String
  field :email, type: String
  field :address, type: String
  field :expired_at, type: DateTime
  field :source, type: String

  has_many :deliveries, autosave: true, dependent: :destroy
  belongs_to :user

  validates :email, presence: true
  validates :phone, presence: true
  validates :deliveries, associated: true
  validates :deliveries, presence: true
  accepts_nested_attributes_for :deliveries

  before_create :create_access_token
  before_create :set_expired_at
  after_create :queue_expire_job
  # after_create :send_confirmation


  def price
    self.deliveries.sum { |d| d.price || 0 } - self.discount
  end

  def discount
    self.deliveries.sum { |d| (d.store.discount_amount if d.discount?) || 0 }
  end

  def create_access_token
    self.access_token = SecureRandom.hex 5
  end

  def send_confirmation
    OrderMailer.confirmation(self.id.to_s).deliver_now
  end

  def items
    Item.in delivery_id: self.deliveries.map(&:id)
  end

  def total_boxes
    self.items.map(&:quantity).reduce(:+)
  end

  def set_expired_at
    email_times = self.deliveries.map(&:email_time)
    self.expired_at = email_times.min # - 10.minutes
  end

  def expired?
    expired_at <= Time.zone.now
  end

  def queue_expire_job
    ExpireJob.set(wait_until: self.expired_at.in_time_zone).perform_later(self.id.to_s)
  end

  def cancel!
    if self.confirmed?
      self.update confirmed_at: nil
      OrderMailer.cancel(self.id.to_s).deliver_later
    else
      true #prevent user from confirming multiple times
    end
  end
end
