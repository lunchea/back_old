class Profile
  include Mongoid::Document
  include Mongoid::Timestamps
  embedded_in :user, inverse_of: :profile
  # other
  field :username, type: String, default: ""
  field :phone, type: String, default: ""
end
