class Item
  include Mongoid::Document
  include Mongoid::Timestamps

  field :quantity, type: Integer
  field :price, type: Money
  field :box_name, type: String

  belongs_to :delivery, inverse_of: :items
  belongs_to :box
  embeds_many :preferences, class_name: "ItemPreference"
  accepts_nested_attributes_for :preferences

  validates :quantity, numericality: { only_integer: true }
  validates :box, associated: true
  before_create :set_price
  before_create :set_box_name

  def set_price
    self.price = self.quantity.to_i * self.box.try(:price).to_f
  end

  def set_box_name
    self.box_name = self.box.name
  end
end
