class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Confirmable
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :recoverabl, :async
  devise :invitable, :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable, :lockable

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  field :locked_at,       type: Time

  index({ email: 1 }, { unique: true })

  searchkick
  has_many :stores, inverse_of: :store_admin, class_name: "Store"
  has_many :orders
  embeds_one :profile, class_name: "Profile"

  after_create 'self.create_profile'
  after_create :update_past_orders
  has_many :feedbacks

  def reset_password!(new_password, new_password_confirmation)
    self.password = new_password
    self.password_confirmation = new_password_confirmation
    save
  end

  def update_past_orders
    UpdateUserOrdersJob.perform_later(self.id.to_s)
  end
end
