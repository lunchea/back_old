class ExpireJob < ActiveJob::Base
  queue_as :mailers

  def perform(order_id)
    @order = Order.find order_id
    if @order and !@order.confirmed? and @order.expired?
      OrderMailer.expire(order_id).deliver_later
    end
  end
end