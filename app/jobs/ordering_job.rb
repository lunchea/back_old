class OrderingJob < ActiveJob::Base
  queue_as :mailers
    
  def perform(store_id)
    @store = Store.find store_id
    @store.send_summary Date.today
    self.class.set(wait_until: @store.next_email_time).perform_later store_id
  end
end
