class UpdateUserOrdersJob < ActiveJob::Base
  queue_as :mailers

  def perform(user_id)
    user = User.find user_id
    orders = Order.where email: user.email
    orders.update_all user_id: user_id
  end

end
