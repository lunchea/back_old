class PartnerJob < ActiveJob::Base
  queue_as :mailers
  
  def date_of_next_weekday(date)
    if (date.saturday?)
      date += 1  #if it's a saturday, change it to a sunday, as the next day will be monday
    elsif (date.friday?)
      date += 2  #if it's a friday, change it to a sunday, as the next day will be monday
    end
    date + 1
  end

  def delivery_time_of(day)
    email_times = Store.all.map { |s| s.email_time_of(day) }
    email_times.max
  end

  def perform(source_id)
    OrderMailer.partner_summary(source_id, Date.today.to_s).deliver_later
    nextWeekday = date_of_next_weekday(Date.today)
    self.class.set(wait_until: delivery_time_of(nextWeekday)).perform_later(source_id)
  end
end