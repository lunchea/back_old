Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.smtp_settings = Rails.application.secrets['mailer']['smtp_settings'].symbolize_keys
  config.action_mailer.default_url_options = Rails.application.secrets['mailer']['default_url_options'].symbolize_keys
  config.action_mailer.default_options = Rails.application.secrets['mailer']['default_options'].symbolize_keys

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log


  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
end
