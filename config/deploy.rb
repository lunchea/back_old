# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'lunchbox'
set :repo_url, 'git@bitbucket.org:lunchea/back.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/lunchbox/app'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug


set :puma_role, :web
set :puma_conf, "#{shared_path}/config/puma.rb"
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"    #accept array for multi-bind

set :rbenv_type, :system # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.1.2'
# set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
# set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

set :pty, false
set :sidekiq_concurrency, 10
set :sidekiq_env, :production
set :rails_env, :production


# Default value for :linked_files is []
set :linked_files, %w{config/mongoid.yml config/secrets.yml config/sidekiq.yml config/skylight.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

namespace :setup do
  desc "Upload database.yml file."
  task :upload_config do
    on roles(:app) do
      fetch(:linked_files).each do |file|
        upload! file, "#{shared_path}/" + file
      end
    end
  end
end