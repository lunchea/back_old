source 'https://rubygems.org'

ruby "2.1.2"

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',      group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',    group: :development

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'
gem 'puma'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
group :development do
  gem 'capistrano', '~> 3.2.0'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rails', '~> 1.0.0'
  gem 'capistrano-rbenv', '~> 2.0'
  gem 'capistrano3-puma'
  gem 'capistrano-sidekiq'
  gem 'railroady'
  gem 'guard'
  gem 'pry', "< 0.10.0"
  gem 'guard-minitest'
  gem 'guard-livereload'
  gem 'timecop'
  gem 'web-console', '~> 2.0'
end

group :development, :test do
  gem 'database_cleaner'
  gem 'faker'
  gem "factory_girl_rails"
  gem 'byebug'
end

# Use debugger
# gem 'debugger', group: [:development, :test]

# database
gem 'mongoid'
gem "bson"

# logic
gem "mongoid-enum"
gem 'money', "~> 6.5.0"
gem 'money-rails', github: "hardywu/money-rails"
gem 'mongoid_alize'

# rest api
gem 'grape'
gem 'grape-entity'
gem 'grape-swagger'
gem 'rack-cors', :require => 'rack/cors'
gem 'jwt'

# model logic
gem 'devise'
gem 'devise_invitable'
gem "pundit"
gem 'rolify'

# Paignator
gem 'kaminari', '~> 0.15.1'

# full text search
gem 'searchkick'

# # redis
# gem 'redis'
# gem 'hiredis'

# # Background jobs
gem 'sidekiq'
gem 'sidetiq'

gem 'sinatra', require: false ## need by sidekiq/web

# Mailer Design
gem 'premailer-rails'
gem 'nokogiri'

# app status detector
gem "skylight"
