# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application

require 'rack/cors'
use Rack::Cors do

  # allow all origins in development
  allow do
    origins '*'
    resource '*',
      headers: :any,
      expose: ['X-Total', 'X-Page-Total', 'X-Per-Page',
               'X-Offset', 'X-Page',
               'access-token', 'expiry', 'token-type', 'uid',
               'client', 'X-Order-Access-Token', 'X-Jwt'],
      methods: [:get, :post, :delete, :put, :options]
  end
end
